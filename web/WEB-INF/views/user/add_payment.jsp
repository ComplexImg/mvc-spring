<%@ page import="by.bsuir.model.User" %>
<%@ page import="by.bsuir.model.Operator" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="user" value="${user}"/>
<%
    User user = (User) pageContext.getAttribute("user");
%>
<form>

    <input name="field_1" value="${user.getId()}">

    <select name="field_2">
        <c:forEach items="${user.getOperators()}" var="operator">
            <%
                Operator operator = (Operator) pageContext.getAttribute("operator");
            %>
            <option value="${operator.getId()}">${operator.getTitle()}</option>

        </c:forEach>
    </select>

    <input type="text" name="field_3" placeholder="payment">
    <br/>
    <br/>

    <button class="add_paymen_form_btn">go</button>

</form>

<script>

    $(".add_paymen_form_btn").click(function () {

        var form = $(this).parents("form");

        $.post("/mvc/user/add_payment", form.serialize(), function (resp) {

            console.log(resp);

        });

        return false;

    })

</script>