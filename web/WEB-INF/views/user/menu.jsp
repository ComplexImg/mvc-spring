<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div>
    <h2>Hi, ${userName}</h2>
    <ul class="list-style1">
        <li class="first"><a href="<c:url value="/user"/>">Main</a></li>
        <li><a href="<c:url value="/user/operators_list"/>">operators list</a></li>
        <li><a href="<c:url value="/user/add_payment"/>">add payment</a></li>
        <li><a href="<c:url value="/user/payments_history"/>">payment history</a></li>
    </ul>
</div>