<%@ page import="by.bsuir.model.User" %>
<%@ page import="by.bsuir.model.Operator" %>
<%@ page import="by.bsuir.model.TariffEntity" %>
<%@ page import="by.bsuir.model.ServiceEntity" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="operator" value="${operator}"/>
<c:set var="user" value="${user}"/>
<c:set var="operatorUserTariffId" value="${operatorUserTariffId}"/>
<%
    User user = (User) pageContext.getAttribute("user");
    Operator operator = (Operator) pageContext.getAttribute("operator");
    Integer operatorUserTariffId = (Integer) pageContext.getAttribute("operatorUserTariffId");
%>
<input name="user_id" type="hidden" value="${user.getId()}">
<input name="operator_id" type="hidden" value="${operator.getId()}">

<select class="select-tariff">
    <c:forEach items="${operator.getTarifs()}" var="tariff">
        <%
            TariffEntity tariff = (TariffEntity) pageContext.getAttribute("tariff");
        %>
        <option value="${tariff.getId()}"
                <% if(tariff.getId().equals(operatorUserTariffId)){%>selected<%}%>>${tariff.getTitle()}</option>

    </c:forEach>
</select>

<% if (operatorUserTariffId != null) {%>
Srvices:

<table style="width: 100%">
    <tr>
        <th>title</th>
        <th>cost</th>
        <th>ussd</th>
        <th>desc</th>
        <th style="width: 20px;">in ?</th>
    </tr>
    <c:forEach items="${operator.getServices()}" var="service">
        <tr>
            <td>${service.getTitle()}</td>
            <td>${service.getCostPerMonth()}</td>
            <td>${service.getUssdCode()}</td>
            <td>${service.getDescription()}</td>
            <td>
                <%
                    ServiceEntity service = (ServiceEntity) pageContext.getAttribute("service");

                %>
                <input data-service_id="${service.getId()}" class="tariff-checkbox" type="checkbox" value=""
                       <%if(user.isServiceInUser(service)){%>checked="checked"<%}%>>
            </td>
        </tr>
    </c:forEach>

</table>

<%}%>
<script>


    $("input[type=checkbox]").click(function () {


        var service_id = $(this).data('service_id');
        var checked = $(this).is(":checked");
        var user_id = $("input[name=user_id]").val();

        var data = {
            field_1: service_id,
            field_2: user_id,
            string_1: checked,
        };

        $.post("/mvc/user/save_or_delete_service_in_user", data, function (resp) {

            console.log(resp);

        });

    });

    $(".select-tariff").change(function () {

        var tariff_id = $(this).val();
        var user_id = $("input[name=user_id]").val();
        var operator_id = $("input[name=operator_id]").val();

        var data = {
            field_1: tariff_id,
            field_2: user_id,
            field_3: operator_id,
        };


        $.post("/mvc/user/update_user_operator_tariff", data, function (resp) {

            console.log(resp);

        });
    })


</script>