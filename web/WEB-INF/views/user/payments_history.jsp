<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table style="width: 100%">
    <tr>
        <th>Amount</th>
        <th>Operator_id</th>
        <th>date</th>
    </tr>

    <c:forEach items="${payments}" var="payment">
        <tr>
            <td>${payment.getAmount()}</td>
            <td>${payment.getOperator_id()}</td>
            <td>${payment.getDate()}</td>
        </tr>
    </c:forEach>
</table>