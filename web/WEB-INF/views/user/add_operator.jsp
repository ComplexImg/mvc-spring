<%@ page import="by.bsuir.model.User" %>
<%@ page import="by.bsuir.model.Operator" %>
<%@ page import="by.bsuir.model.TariffEntity" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="operator" value="${operator}"/>
<c:set var="user" value="${user}"/>
<%
    User user = (User) pageContext.getAttribute("user");
    Operator operator = (Operator) pageContext.getAttribute("operator");
%>
<form>

    <input name="field_1" type="hidden" value="${user.getId()}">
    <input name="field_2" type="hidden" value="${operator.getId()}">

    <select name="field_3">
        <c:forEach items="${operator.getTarifs()}" var="tariff">
            <%
                TariffEntity tariff = (TariffEntity) pageContext.getAttribute("tariff");
            %>
            <option value="${tariff.getId()}">${tariff.getTitle()}</option>

        </c:forEach>
    </select>

    <button id="btn">Add</button>
</form>
Srvices:

<table style="width: 100%">
    <tr>
        <th>title</th>
        <th>cost</th>
        <th>ussd</th>
        <th>desc</th>
    </tr>
    <c:forEach items="${operator.getServices()}" var="service">
        <tr>
            <td>${service.getTitle()}</td>
            <td>${service.getCostPerMonth()}</td>
            <td>${service.getUssdCode()}</td>
            <td>${service.getDescription()}</td>
        </tr>
    </c:forEach>

</table>

<script>

    $("#btn").click(function () {

        var form = $(this).parents("form");

        $.post("/mvc/user/add_operator", form.serialize(), function (resp) {

            console.log(resp);

        });

        return false;

    })

</script>
