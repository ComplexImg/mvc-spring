<%@ page import="by.bsuir.model.User" %>
<%@ page import="by.bsuir.model.Operator" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="operatorsList" value="${operatorsList}"/>
<c:set var="user" value="${user}"/>
<%
    User user = (User) pageContext.getAttribute("user");
%>

<div id="operator_list">

    <!-- Swiper -->
    <div class="swiper-container">
        <div class="swiper-wrapper">

            <c:forEach items="${operatorsList}" var="operator">

                <div class="swiper-slide">
                    <div class="my-slide-wrapper">
                        <div class="slide-header">
                                ${operator.getTitle()}
                        </div>
                        <div class="slide-content">
                                ${operator.getDescription()}
                            <div class="slide-btn">
                                <%
                                    Operator operator = (Operator) pageContext.getAttribute("operator");
                                    if (user.isOperatorInUser(operator)) {
                                %>
                                <a href="<c:url value="/user/edit_operator/${operator.getId()}"/>">edit</a>
                                <%
                                } else {
                                %>
                                <a href="<c:url value="/user/add_operator/${operator.getId()}"/>">add</a>
                                <%
                                    }
                                %>

                            </div>

                        </div>
                        <div class="slide-footer">
                                ${operator.getEmail()} -
                                ${operator.getDate()}
                        </div>
                    </div>
                </div>

            </c:forEach>

        </div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>

    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper('.swiper-container', {

            spaceBetween: 0,
            slidesPerGroup: 1,
            slidesPerView: 1,
            loop: true,
            loopFillGroupWithBlank: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    </script>
</div>