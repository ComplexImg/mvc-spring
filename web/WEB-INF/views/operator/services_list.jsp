<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table style="width: 100%">
    <tr>
        <th>title</th>
        <th>costPerMonth</th>
        <th>description</th>
        <th>UssdCode</th>
        <th>edit</th>
        <th>delete</th>
    </tr>

    <c:forEach items="${servicesList}" var="service">
        <tr>
            <td>${service.getTitle()}</td>
            <td>${service.getCostPerMonth()}</td>
            <td>${service.getUssdCode()}</td>
            <td>${service.getDescription()}</td>
            <td>
                <a href="<c:url value="/operator/edit_service/${service.getId()}"/>">edit</a>
            </td>
            <td>
                <button class="delete" data-deleted_id="${service.getId()}">Delete</button>
            </td>
        </tr>
    </c:forEach>
</table>

<script>


    $("button[class=delete]").click(function () {

        var deleted_id = $(this).data("deleted_id");

        alert(deleted_id);

        $.post("/mvc/operator/delete_service/" + deleted_id, {} , function (answer) {

            console.log(answer);

        });

    });

</script>

