<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table style="width: 100%">
    <tr>
        <th>title</th>
        <th>costPerMonth</th>
        <th>description</th>
        <th>edit</th>
        <th>delete</th>
    </tr>

    <c:forEach items="${tariffsList}" var="tariff">
        <tr>
            <td>${tariff.getTitle()}</td>
            <td>${tariff.getCostPerMonth()}</td>
            <td>${tariff.getDescription()}</td>
            <td>
                <a href="<c:url value="/operator/edit_tariff/${tariff.getId()}"/>">edit</a>
            </td>
            <td>
                <button class="delete" data-deleted_id="${tariff.getId()}">Delete</button>
            </td>
        </tr>
    </c:forEach>
</table>

<script>


    $("button[class=delete]").click(function () {

        var deleted_id = $(this).data("deleted_id");

        alert(deleted_id);

        $.post("/mvc/operator/delete_tariff/" + deleted_id, {} , function (answer) {

            console.log(answer);

        });

    });

</script>

