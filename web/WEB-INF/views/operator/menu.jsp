<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<ul>
    <li><a href="<c:url value="/operator"/>">Main</a></li>
    <li><a href="<c:url value="/operator/services_list"/>">services list</a></li>
    <li><a href="<c:url value="/operator/add_service"/>">add service</a></li>
    <li><a href="<c:url value="/operator/tariffs_list"/>">tariffs list</a></li>
    <li><a href="<c:url value="/operator/add_tariff"/>">add tariff</a></li>
    <li><a href="<c:url value="/operator/edit_info"/>">edit info</a></li>
</ul>