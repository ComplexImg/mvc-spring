<%@ page import="by.bsuir.model.TariffEntity" %>
<%@ page import="by.bsuir.model.ServiceEntity" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="tariffEntity" value="${tariffEntity}"/>
<form class="form">
    <input type="hidden" name="id" value="${tariffEntity.getId()}"/><br/><br/>
    <input type="text" name="title" placeholder="Title" value="${tariffEntity.getTitle()}"/><br/><br/>
    <input type="text" name="description" placeholder="description" value="${tariffEntity.getDescription()}"/><br/><br/>
    <input type="text" name="costPerMonth" placeholder="costPerMonth"
           value="${tariffEntity.getCostPerMonth()}"/><br/><br/>
    <button name="regSubmit">Register operator</button>
</form>

<c:if test="${not empty tariffEntity}">

Srvices:

<table style="width: 100%">
    <tr>
        <th>title</th>
        <th>cost</th>
        <th>ussd</th>
        <th>desc</th>
        <th>in ?</th>
    </tr>
    <c:forEach items="${servicesList}" var="service">
        <tr>
            <td>${service.getTitle()}</td>
            <td>${service.getCostPerMonth()}</td>
            <td>${service.getUssdCode()}</td>
            <td>${service.getDescription()}</td>
            <td>
                <%
                    ServiceEntity service = (ServiceEntity) pageContext.getAttribute("service");
                    TariffEntity te = (TariffEntity) pageContext.getAttribute("tariffEntity");
                    te.isServiceInTariffServices(service);
                %>
                <input data-service_id="${service.getId()}" class="tariff-checkbox" type="checkbox" value=""
                       <%if(te.isServiceInTariffServices(service)){%>checked="checked"<%}%>>
            </td>
        </tr>
    </c:forEach>

</table>

</c:if>


<script>


    $("input[type=checkbox]").click(function () {


        var service_id = $(this).data('service_id');
        var checked = $(this).is(":checked");
        var tariff_id = $(".form input[name=id]").val();

        var data = {
            field_1: service_id,
            field_2: tariff_id,
            string_1: checked,
        };

        $.post("/mvc/operator/save_or_delete_service_in_tariff", data, function (resp) {

            console.log(resp);

        });

    });


    $("button[name=regSubmit]").click(function () {

        var form = $(this).parents("form");

        $.post("/mvc/operator/save_tariff", form.serialize(), function (answer) {

            console.log(answer);

        });

        return false;

    });

</script>