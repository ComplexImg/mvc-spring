<form class="form">

    <input type="hidden" name="id" placeholder="Title" value="${serviceEntity.getId()}"/><br/><br/>
    <input type="text" name="title" placeholder="Title" value="${serviceEntity.getTitle()}"/><br/><br/>
    <input type="text" name="description" placeholder="description" value="${serviceEntity.getDescription()}"/><br/><br/>
    <input type="text" name="ussdCode" placeholder="ussdCode"  value="${serviceEntity.getUssdCode()}"/><br/><br/>
    <input type="text" name="costPerMonth" placeholder="costPerMonth" value="${serviceEntity.getCostPerMonth()}"/><br/><br/>

    <button name="regSubmit">Register operator</button>

</form>


<script>
    $("button[name=regSubmit]").click(function () {

        var form = $(this).parents("form");

        $.post("/mvc/operator/save_service", form.serialize() , function (answer) {

            console.log(answer);

        });

        return false;

    });

</script>
