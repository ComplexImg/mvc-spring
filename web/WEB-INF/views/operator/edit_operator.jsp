<form class="form">

    <input type="hidden" name="id" placeholder="Login"  value="${operatorInfo.getId()}"/><br/><br/>
    <input type="hidden" name="username"  value="${operatorInfo.getUsername()}"/><br/><br/>
    <input type="text" name="email" placeholder="Email" value="${operatorInfo.getEmail()}"/><br/><br/>
    <input type="text" name="password" placeholder="Pass" value="${operatorInfo.getPassword()}"/><br/><br/>
    <input type="text" name="title" placeholder="Title" value="${operatorInfo.getTitle()}"/><br/><br/>
    <input type="text" name="description" placeholder="description" value="${operatorInfo.getDescription()}"/><br/><br/>
    <input type="hidden" name="type" value="operator">
    <button name="regSubmit">Register operator</button>

</form>


<script>
    $("button[name=regSubmit]").click(function () {

        var form = $(this).parents("form");

        $.post("/mvc/operator/save_operator", form.serialize() , function (answer) {

            console.log(answer);

        });

        return false;

    });

</script>
