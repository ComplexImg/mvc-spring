<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<ul>
    <li><a href="<c:url value="/admin"/>">Main</a></li>
    <li><a href="<c:url value="/admin/operators_list"/>">Operators list</a></li>
    <li><a href="<c:url value="/admin/new_operator"/>">New operator</a></li>
</ul>