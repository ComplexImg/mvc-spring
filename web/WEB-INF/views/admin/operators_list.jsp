<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table style="width: 100%">
    <tr>
        <th>Username</th>
        <th>email</th>
        <th>date</th>
        <th>title</th>
        <th>desc</th>
        <th>delete</th>
    </tr>

    <c:forEach items="${operatorsList}" var="operator">
        <tr>
            <td>${operator.getUsername()}</td>
            <td>${operator.getEmail()}</td>
            <td>${operator.getDate()}</td>
            <td>${operator.getTitle()}</td>
            <td>${operator.getDescription()}</td>
            <td>
                <button class="delete" data-deleted_id="${operator.getId()}">Delete</button>
            </td>
        </tr>
    </c:forEach>
</table>

<script>


    $("button[class=delete]").click(function () {

        var deleted_id = $(this).data("deleted_id");

        alert(deleted_id);

        $.post("/mvc/admin/delete_operator/" + deleted_id, {} , function (answer) {

            console.log(answer);

        });

    });

</script>

