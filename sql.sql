CREATE TABLE IF NOT EXISTS `mydb`.`service` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `description` TINYTEXT NULL,
  `operator_id` INT NOT NULL,
  `cost_per_month` INT NOT NULL,
  `USSD_code` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `operator_id_idx` (`operator_id` ASC),
  CONSTRAINT `operator_id`
    FOREIGN KEY (`operator_id`)
    REFERENCES `mydb`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



CREATE TABLE IF NOT EXISTS `mydb`.`tariff_services` (
  `tariff_id` INT NOT NULL,
  `service_id` INT NOT NULL,
  PRIMARY KEY (`tariff_id`, `service_id`),
  INDEX `service_id_idx` (`service_id` ASC),
  CONSTRAINT `t_s_tariff_id`
    FOREIGN KEY (`tariff_id`)
    REFERENCES `mydb`.`tariff` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `t_s_service_id`
    FOREIGN KEY (`service_id`)
    REFERENCES `mydb`.`service` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



CREATE TABLE IF NOT EXISTS `mydb`.`user_services` (
  `s_user_id` INT NOT NULL,
  `s_service_id` INT NOT NULL,
  `date` DATETIME NULL,
  INDEX `service_id_idx` (`s_service_id` ASC),
  PRIMARY KEY (`s_service_id`, `s_user_id`),
  CONSTRAINT `u_s_user_id`
    FOREIGN KEY (`s_user_id`)
    REFERENCES `mydb`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `u_s_service_id`
    FOREIGN KEY (`s_service_id`)
    REFERENCES `mydb`.`service` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



CREATE TABLE IF NOT EXISTS `mydb`.`payment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `operator_id` INT NOT NULL,
  `amount` INT NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `user_id_idx` (`user_id` ASC),
  INDEX `operator_id_idx` (`operator_id` ASC),
  CONSTRAINT `p_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `mydb`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `p_operator_id`
    FOREIGN KEY (`operator_id`)
    REFERENCES `mydb`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `mydb`.`operator_info` (
  `id` INT NOT NULL,
  `title` VARCHAR(45) NULL,
  `description` MEDIUMTEXT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `user_id_UNIQUE` (`id` ASC),
  CONSTRAINT `o_i_user_id`
    FOREIGN KEY (`id`)
    REFERENCES `mydb`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
