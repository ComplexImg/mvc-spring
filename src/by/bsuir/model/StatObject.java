package by.bsuir.model;

/**
 * Created by Tom on 16.04.2018.
 */
public class StatObject {

    private Integer field_1;
    private Integer field_2;
    private Integer field_3;
    private String string_1;
    private String string_2;
    private String string_3;

    public Integer getField_1() {
        return field_1;
    }

    public void setField_1(Integer field_1) {
        this.field_1 = field_1;
    }

    public Integer getField_2() {
        return field_2;
    }

    public void setField_2(Integer field_2) {
        this.field_2 = field_2;
    }

    public Integer getField_3() {
        return field_3;
    }

    public void setField_3(Integer field_3) {
        this.field_3 = field_3;
    }

    public String getString_1() {
        return string_1;
    }

    public void setString_1(String string_1) {
        this.string_1 = string_1;
    }

    public String getString_2() {
        return string_2;
    }

    public void setString_2(String string_2) {
        this.string_2 = string_2;
    }

    public String getString_3() {
        return string_3;
    }

    public void setString_3(String string_3) {
        this.string_3 = string_3;
    }
}
