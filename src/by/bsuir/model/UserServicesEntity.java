package by.bsuir.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Tom on 14.03.2018.
 */
@Entity
@Table(name = "user_services", schema = "mydb", catalog = "")
@IdClass(UserServicesEntityPK.class)
public class UserServicesEntity {
    private int sUserId;
    private int sServiceId;
    private Timestamp date;

    @Id
    @Column(name = "s_user_id")
    public int getsUserId() {
        return sUserId;
    }

    public void setsUserId(int sUserId) {
        this.sUserId = sUserId;
    }

    @Id
    @Column(name = "s_service_id")
    public int getsServiceId() {
        return sServiceId;
    }

    public void setsServiceId(int sServiceId) {
        this.sServiceId = sServiceId;
    }

    @Basic
    @Column(name = "date")
    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserServicesEntity that = (UserServicesEntity) o;

        if (sUserId != that.sUserId) return false;
        if (sServiceId != that.sServiceId) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = sUserId;
        result = 31 * result + sServiceId;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }
}
