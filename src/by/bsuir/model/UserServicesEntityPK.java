package by.bsuir.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Tom on 14.03.2018.
 */
public class UserServicesEntityPK implements Serializable {
    private int sUserId;
    private int sServiceId;

    @Column(name = "s_user_id")
    @Id
    public int getsUserId() {
        return sUserId;
    }

    public void setsUserId(int sUserId) {
        this.sUserId = sUserId;
    }

    @Column(name = "s_service_id")
    @Id
    public int getsServiceId() {
        return sServiceId;
    }

    public void setsServiceId(int sServiceId) {
        this.sServiceId = sServiceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserServicesEntityPK that = (UserServicesEntityPK) o;

        if (sUserId != that.sUserId) return false;
        if (sServiceId != that.sServiceId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = sUserId;
        result = 31 * result + sServiceId;
        return result;
    }
}
