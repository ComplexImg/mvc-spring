package by.bsuir.model;

import javax.persistence.*;

/**
 * Created by Tom on 14.03.2018.
 */
@Entity
@Table(name = "tariff_services", schema = "mydb", catalog = "")
@IdClass(TariffServicesEntityPK.class)
public class TariffServicesEntity {
    private int tariffId;
    private int serviceId;

    @Id
    @Column(name = "tariff_id")
    public int getTariffId() {
        return tariffId;
    }

    public void setTariffId(int tariffId) {
        this.tariffId = tariffId;
    }

    @Id
    @Column(name = "service_id")
    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TariffServicesEntity that = (TariffServicesEntity) o;

        if (tariffId != that.tariffId) return false;
        if (serviceId != that.serviceId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tariffId;
        result = 31 * result + serviceId;
        return result;
    }
}
