package by.bsuir.service;


import by.bsuir.model.Operator;
import by.bsuir.model.PaymentEntity;
import by.bsuir.model.User;

import java.util.List;

/**
 * Created by Tom on 02.04.2018.
 */
public interface PaymentService {

    public PaymentEntity addPayment(PaymentEntity paymentEntity);

    public List<PaymentEntity> getUserPayments(Integer user_id) ;

}
