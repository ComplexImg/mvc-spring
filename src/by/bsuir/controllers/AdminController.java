package by.bsuir.controllers;

import by.bsuir.model.Operator;
import by.bsuir.model.TariffEntity;
import by.bsuir.model.User;
import by.bsuir.model.UsersEntity;
import by.bsuir.service.TariffService;
import by.bsuir.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class AdminController extends BaseController {

    @Autowired
    UserService userService;

    @Autowired
    TariffService tariffService;


    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String index(ModelMap modelMap)
    {

        String user = this.authUser();

        System.out.print(user);


        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "admin/index.jsp");
        return "Template";
    }


    @RequestMapping(value = "/admin/operators_list", method = RequestMethod.GET)
    public String operatorsList(ModelMap modelMap)
    {
        List<Operator> operatorsList = userService.getOperatorsList();

        modelMap.addAttribute("operatorsList", operatorsList);
        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "admin/operators_list.jsp");
        return "Template";
    }


    // стр с формой добавления
    @RequestMapping(value = "/admin/new_operator", method = RequestMethod.GET)
    public String addOperator(ModelMap modelMap)
    {
        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "admin/new_operator.jsp");
        return "Template";
    }


    @RequestMapping(value = "/admin/save_operator", method = RequestMethod.POST)
    @ResponseBody
    public Operator addOperatorPOST(ModelMap modelMap, @ModelAttribute Operator operator) {
        if (operator.isValid()) {
            UsersEntity findUser = userService.getUserByLogin(operator.getUsername());

            if(findUser == null){
                Operator newUser = (Operator) userService.saveUser(operator);
                return newUser;
            }
        }
        return null;
    }


    @RequestMapping(value = "/admin/delete_operator/{id}", method = RequestMethod.POST)
    public boolean deleteOperator(@PathVariable Integer id, ModelMap modelMap) {
        // temp
        UsersEntity usersEntity = userService.getUserById(id);

        if(usersEntity != null){
            userService.delete(usersEntity);
            return true;
        }

        return false;
    }

}
