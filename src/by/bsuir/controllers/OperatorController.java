package by.bsuir.controllers;

import by.bsuir.model.*;
import by.bsuir.service.ServicesService;
import by.bsuir.service.TariffService;
import by.bsuir.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@Controller
public class OperatorController extends BaseController{

    @Autowired
    ServicesService servicesService;
    @Autowired
    UserService userService;
    @Autowired
    TariffService tariffService;


    @RequestMapping(value = "/operator/users_list", method = RequestMethod.GET)
    public String usersList(ModelMap modelMap) {

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/users_list.jsp");
        return "Template";
    }


    //////////////////// ok

    @RequestMapping(value = "/operator/edit_info", method = RequestMethod.GET)
    public String editOperatorInfo(ModelMap modelMap) {

        Operator operatorInfo = userService.getOperatorByLogin(this.authUser());

        modelMap.addAttribute("operatorInfo", operatorInfo);

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/edit_operator.jsp");
        return "Template";
    }


    @RequestMapping(value = "/operator/save_operator", method = RequestMethod.POST)
    @ResponseBody
    public Operator editOperatorPOST(ModelMap modelMap, @ModelAttribute Operator operator) {
        if (operator.isValid()) {

            Operator oldOperator = userService.getOperatorById(operator.getId());

            operator.setServices(oldOperator.getServices());
            operator.setTarifs(oldOperator.getTarifs());

            Operator newUser = (Operator) userService.saveUser(operator);
            return newUser;
        }
        return null;
    }


    @RequestMapping(value = "/operator", method = RequestMethod.GET)
    public String index(ModelMap modelMap) {
        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/index.jsp");
        return "Template";
    }


    @RequestMapping(value = "/operator/services_list", method = RequestMethod.GET)
    public String servicesList(ModelMap modelMap) {

        Operator operator = userService.getOperatorByLogin(this.authUser());

        List<ServiceEntity> servicesList = servicesService.getOperatorServices(operator);
        modelMap.addAttribute("servicesList", servicesList);

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/services_list.jsp");
        return "Template";
    }


    @RequestMapping(value = "/operator/edit_service/{id}", method = RequestMethod.GET)
    public String editService(@PathVariable Integer id, ModelMap modelMap) {

        System.out.println(id);

        ServiceEntity serviceEntity = servicesService.getById(id);

        modelMap.addAttribute("serviceEntity", serviceEntity);

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/add_service.jsp");
        return "Template";
    }


    @RequestMapping(value = "/operator/add_service", method = RequestMethod.GET)
    public String addService(ModelMap modelMap) {

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/add_service.jsp");
        return "Template";
    }


    // список тарифов с возм удаления
    @RequestMapping(value = "/operator/tariffs_list", method = RequestMethod.GET)
    public String tariffsList(ModelMap modelMap) {

        Operator operator = userService.getOperatorByLogin(this.authUser());

        List<TariffEntity> tariffs = tariffService.getOperatorTariffs(operator);

        System.out.println(tariffs.size());

        modelMap.addAttribute("tariffsList", tariffs);

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/tariffs_list.jsp");
        return "Template";
    }


    @RequestMapping(value = "/operator/edit_tariff/{id}", method = RequestMethod.GET)
    public String editTariff(@PathVariable Integer id, ModelMap modelMap) {

        System.out.println(id);
        TariffEntity tariffEntity = tariffService.getById(id);
        modelMap.addAttribute("tariffEntity", tariffEntity);

        Operator operator = userService.getOperatorByLogin(this.authUser());

        List<ServiceEntity> servicesList = servicesService.getOperatorServices(operator);

        modelMap.addAttribute("servicesList", servicesList);


        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/add_tariff.jsp");
        return "Template";
    }


    @RequestMapping(value = "/operator/add_tariff", method = RequestMethod.GET)
    public String addTariff(ModelMap modelMap) {

        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "operator/add_tariff.jsp");
        return "Template";
    }


    @RequestMapping(value = "/operator/save_service", method = RequestMethod.POST)
    @ResponseBody
    public ServiceEntity addOperatorPOST(ModelMap modelMap, @ModelAttribute ServiceEntity serviceEntity) {
        if (serviceEntity.isValid()) {

            // temp
            Operator operator = userService.getOperatorByLogin(this.authUser());
            serviceEntity.setOwner(operator);
            return servicesService.save(serviceEntity);
        }
        return null;
    }


    @RequestMapping(value = "/operator/delete_tariff/{id}", method = RequestMethod.POST)
    public boolean deleteTariff(@PathVariable Integer id, ModelMap modelMap) {
        // temp
        Operator operator = userService.getOperatorByLogin(this.authUser());
        TariffEntity tariffEntity = tariffService.getById(id);

        if (tariffEntity != null && tariffEntity.getOwner().getId().equals(operator.getId())) {
            return tariffService.delete(tariffEntity);
        }

        return false;
    }


    @RequestMapping(value = "/operator/delete_service/{id}", method = RequestMethod.POST)
    public boolean deleteService(@PathVariable Integer id, ModelMap modelMap) {
        // temp
        Operator operator = userService.getOperatorByLogin(this.authUser());

        ServiceEntity serviceEntity = servicesService.getById(id);

        if (serviceEntity != null && serviceEntity.getOwner().getId().equals(operator.getId())) {
            return servicesService.delete(serviceEntity);
        }

        return false;
    }


    @RequestMapping(value = "/operator/save_tariff", method = RequestMethod.POST)
    @ResponseBody
    public TariffEntity addTariffPOST(ModelMap modelMap, @ModelAttribute TariffEntity tariffEntity) {
        if (tariffEntity.isValid()) {

            if (tariffEntity.getId() != null) {
                TariffEntity oldTariff = tariffService.getById(tariffEntity.getId());
                tariffEntity.setServices(oldTariff.getServices());
            }


            // temp
            Operator operator = userService.getOperatorByLogin(this.authUser());

            tariffEntity.setOwner(operator);


            return tariffService.save(tariffEntity);
        }
        return null;
    }


    @RequestMapping(value = "/operator/save_or_delete_service_in_tariff", method = RequestMethod.POST)
    @ResponseBody
    public boolean addOrDeleteServiceInTariffPOST(ModelMap modelMap, @ModelAttribute StatObject statObject) {

        TariffEntity tariffEntity = tariffService.getById(statObject.getField_2());

        if (statObject.getString_1().equals("false")) {
            for (ServiceEntity serviceEntity : tariffEntity.getServices()) {
                if (serviceEntity.getId().equals(statObject.getField_1())) {
                    tariffEntity.getServices().remove(serviceEntity);
                    break;
                }
            }
        } else {
            ServiceEntity serviceEntity = servicesService.getById(statObject.getField_1());
            tariffEntity.getServices().add(serviceEntity);
        }

        tariffService.save(tariffEntity);

        return true;
    }


}
