package by.bsuir.controllers;


import by.bsuir.model.Operator;
import by.bsuir.model.User;
import by.bsuir.model.UsersEntity;
import by.bsuir.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController extends BaseController {

    @Autowired
    UserService userService;


    @RequestMapping(value = {"/index", "/", ""}, method = RequestMethod.GET)
    public String index(ModelMap modelMap) {

        List<Operator> operators = userService.getOperatorsList();

//        System.out.print(operators.size() +  "--" + userService.getOperatorsList2().size());
        modelMap.addAttribute("var", "_____________");

        modelMap.addAttribute("title", "Main");
        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "main/index.jsp");
        return "Template";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginGet(ModelMap modelMap) {
        modelMap.addAttribute("var", "_____________");
        modelMap.addAttribute("title", "Login");
        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "main/login.jsp");
        return "Template";
    }


    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public String noAccess(ModelMap modelMap) {


        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "main/403.jsp");
        return "Template";
    }

    @RequestMapping(value = "/404", method = RequestMethod.GET)
    public String noPage(ModelMap modelMap) {
        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("view", "main/404.jsp");
        return "Template";
    }


    //////////////////////// OK

    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String about(ModelMap modelMap) {
        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("title", "About");
        modelMap.addAttribute("view", "main/about.jsp");
        return "Template";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String registerGet(ModelMap modelMap) {
        modelMap.addAttribute("userName", this.authUser());
        modelMap.addAttribute("menu", this.getMenuForUser());
        modelMap.addAttribute("title", "Register");
        modelMap.addAttribute("view", "main/register.jsp");
        return "Template";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public User registerPost(ModelMap modelMap, @ModelAttribute User user) {
        if (user.isValid()) {
            UsersEntity findUser = userService.getUserByLogin(user.getUsername());

            if (findUser == null) {
                User newUser = (User) userService.saveUser(user);
                return newUser;
            }
        }
        return null;
    }

}
