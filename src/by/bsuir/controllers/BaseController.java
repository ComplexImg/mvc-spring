package by.bsuir.controllers;

import by.bsuir.model.User;
import by.bsuir.model.UsersEntity;
import by.bsuir.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Service
public class BaseController {

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    UserService userService;

    public String authUser()
    {
        try {
            Principal p = httpServletRequest.getUserPrincipal();
            return p.getName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getMenuForUser()
    {
        if(this.authUser() != null){
            UsersEntity user = userService.getUserByLogin(this.authUser());

            if(user.getType().equals("operator")){
                return "operator/menu.jsp";
            }else if(user.getType().equals("user")){
                return "user/menu.jsp";
            }else{
                return "admin/menu.jsp";
            }

        }
        return null;
    }

}
